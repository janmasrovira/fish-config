function fish_greeting
  date +"%a %b %d %R:%S %Z %Y" # same as default but 24h format instead of pm/am
  echo "My pid is" %self
end
